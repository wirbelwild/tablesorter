/**
 * Bit&Black 2017-2019
 */
(function($) {

    let instances = [];

    /**
     * Validates a number
     * @param number
     * @returns {*}
     */
    const isNumber = number => /^-?[\d.]+(?:e-?\d+)?$/.test(number);

    /**
     * Sorts a column
     * @param a
     * @param b
     * @param orderBy
     * @returns {number}
     */
    const sortColumn = (a, b, orderBy) => {
                                                            
        // Use data-value if exists
        let aValue = $(a).attr("data-sort-value") 
            ? $(a).attr("data-sort-value") 
            : $(a).html()
        ;
        let bValue = $(b).attr("data-sort-value") 
            ? $(b).attr("data-sort-value") 
            : $(b).html()
        ;

        // Convert to lower case
        aValue = aValue.toLowerCase();
        bValue = bValue.toLowerCase();

        // Convert string to number
        if (isNumber(aValue)) {
            aValue = aValue * 1;
        }
        
        if (isNumber(bValue)) {
            bValue = bValue * 1;
        }

        let order = (aValue > bValue) ? -1 : 1;

        if (orderBy === "asc") {
            order = (aValue < bValue) ? -1 : 1; 
        }
        
        return order;
    };

    /**
     * Sorts the table
     * @param element
     * @param options
     * @returns {*|jQuery|HTMLElement}
     */
    const sortTable = function (element, options) {

        let hasLoaded = false;

        let settings = {
            sortFirst: 0,
            headers: {},
            callbacks: {
                onsortstart() {},
                onsortfinish() {},
            },
            callbackBehaviour: {
                onsortstart: "continue"
            }
        };

        $.extend(settings, options);
            
        $("> thead th", element)
            .attr("data-sort-function", "enabled")
            .each(function() {

                let indexElement = $(this).index();

                if (settings.hasOwnProperty("headers")) { 
                                   
                    if (settings["headers"].hasOwnProperty(indexElement)) { 
                                         
                        if (settings.headers.hasOwnProperty(indexElement) 
                            && settings.headers[indexElement].hasOwnProperty("sorter") 
                            && settings.headers[indexElement]["sorter"] === false
                        ) { 
                            $(this).attr("data-sort-function", "disabled");
                        }
                    }
                }
                
                if (indexElement === settings.sortFirst) {
                    $(this).attr("data-sort-first", true);
                }
            })
        ;
        
        $("> thead th[data-sort-function='enabled']", element)
            .click(function(event, hasLoaded) {

                let indexColumn = $(this).index() + 1;
                let orderBy = $(this).attr("data-sort-order-by");
                let sortBy = $(this).attr("data-sort-sort-by");
                let column = $("> tbody td:nth-child(" + indexColumn + ")", element);
                let options = {
                    orderBy,
                    sortBy
                };

                // Callback
                if (hasLoaded !== true) {
                    if (settings.callbacks.hasOwnProperty("onsortstart") 
                        && typeof settings.callbacks.onsortstart === "function"
                    ) {
                        settings.callbacks.onsortstart(options);
                    }
                }

                // Sort or don't sort in order to setting
                if (settings.callbackBehaviour.hasOwnProperty("onsortstart") 
                    && settings.callbackBehaviour.onsortstart !== "break"
                ) {
                    
                    column
                        .sort(function(a, b) {
                            return sortColumn(a, b, orderBy);
                        })
                        .each(function() {
                            let $row = $(this).closest("tr");
                            $("tbody", element).first().append($row);
                        })
                    ;

                    // Resort slave elements to their master
                    $("tr[data-sort-pair-master]", element)
                        .each(function() {
                            let id = $(this).attr("data-sort-pair-id");
                            $("tr[data-sort-pair-slave][data-sort-pair-id='" + id + "']", element)
                                .detach()
                                .insertAfter($(this))
                            ;
                        })
                    ;
                }

                // Change sort setting and arrow
                if ($(this).attr("data-sort-order-by") === "asc") {
                    $(this)
                        .attr("data-sort-order-by", "desc")
                        .toggleArrow("&#9650;")
                    ;
                } else {
                    $(this)
                        .attr("data-sort-order-by", "asc")
                        .toggleArrow("&#9660;")
                    ;
                }

                // Callback
                if (settings.callbacks.hasOwnProperty("onsortfinish") && typeof settings.callbacks.onsortfinish === "function") {
                    settings.callbacks.onsortfinish(options);
                }
            })
            .css("position", "relative") 
            .append("<span class='tablesorter-arrow' data-sort-timestamp='" + Date.now() + "'></span>")
            .find(".tablesorter-arrow")
            .html("&#9660;")
            .css("visibility", "hidden")
        ;

        $(element).sorterReady();
    
        return $(element);
    };
    
    /**
     * Keeps chain and adds elements to exe function only once
     * @param options
     * @returns {*}
     */
    function tableSorter(options) {
        return this.each(function () {
            if ($.inArray(JSON.stringify($(this)), instances) === -1) {
                sortTable($(this), options);
                instances.push(JSON.stringify($(this)));
            }
        });
    }
    
    /**
     * Triggers the click
     */
    function sorterReady() {
        $(this)
            .find("[data-sort-first='true']")
            .trigger("click", [true])
        ;
    }

    /**
     * Changes the arrows
     * @param icon
     * @returns {jQuery}
     */
    function toggleArrow(icon) {
        $(this)
            .parents("thead")
            .find(".tablesorter-arrow")
            .css("visibility", "hidden")
        ;

        $(this)
            .find(".tablesorter-arrow")
            .html(icon)
            .css("visibility", "visible")
            .attr("data-sort-function-active", true)
        ;

        return this;
    }

    $.fn.toggleArrow = toggleArrow;
    $.fn.sorterReady = sorterReady;
    $.fn.tableSorter = tableSorter;
}(jQuery));
