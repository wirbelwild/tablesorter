[![Codacy Badge](https://api.codacy.com/project/badge/Grade/6a02c0a7b37643f1a9bbffa4271a538e)](https://www.codacy.com/app/tobias.koengeter/tablesorter?utm_source=wirbelwild@bitbucket.org&amp;utm_medium=referral&amp;utm_content=wirbelwild/tablesorter&amp;utm_campaign=Badge_Grade)

# tableSorter

A small jquery plugin to sort tables

## Installation

This package is available for [node](https://www.npmjs.com/package/bitandblack-table-sorter) and also for [composer](https://packagist.org/packages/bitandblack/table-sorter).

### NPM

Install TableSorter by running `$ npm install bitandblack-table-sorter`.

### Yarn

Install TableSorter by running `$ yarn add bitandblack-table-sorter`.

### Composer

Install TableSorter by running `$ composer require bitandblack/table-sorter` and `$ composer update`.

## How to use 

### Init

TableSorter needs to be initialised inside the jQuery ready function:

```javascript
$(document)
    .ready(function() {
        $('#yourtable').tableSorter();
    })
; 
```

### HTML Tables 

TableSorter needs a classical table including `thead` and `tbody`:

```html
<table id="yourtable">
    <thead>
        <tr>
            <th>Row 1, Cell 1</th>
            <th>Row 2, Cell 1</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Row 1, Cell 2</td>
            <td>Row 2, Cell 2</td>
        </tr>
        <tr>
            <td>Row 1, Cell 3</td>
            <td>Row 2, Cell 3</td>
        </tr>
    </tbody>
</table>
```

### Optional 

#### Sort value

If you want to sort your table differently from your table row values, you only need to add the `data-sort-value` attribute to your table cells. It is possible to change these values dynamically, TableSorter will find them from its own. 

#### Disable columns 

You can disable columns for getting sorted. Init TableSorter like this:

```javascript
$(document)
    .ready(function() { 
        $('#yourtable')
            .tableSorter({
                headers: { 
                    0: { 
                        sorter: false 
                    }
                }
            })
        ;
    })
; 
```

#### Keep rows together

If you want to keep two or more rows together no matter how the sort option is, you only have to add two attributes: the `data-sort-pair-id` and `data-sort-pair-master` respectively `data-sort-pair-slave`. 
Then it should look like this: 

```
<table id="yourtable">
    <thead>
        <tr>
            <th>row10</th>
            <th>row20</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>row11</td>
            <td>row21</td>
        </tr>
        <tr data-sort-pair-id="1" data-sort-pair-master>
            <td>row12</td>
            <td>row22</td>
        </tr>
        <tr data-sort-pair-id="1" data-sort-pair-slave>
            <td>row13</td>
            <td>row23</td>
        </tr>
        <tr>
            <td>row14</td>
            <td>row24</td>
        </tr>
    </tbody>
</table>
```

#### Callbacks

The give you more control, you can use the callbacks `onsortstart` and `onsortfinish`. 
If you set `callbackBehaviour` with `onsortstart` to `break`, tableSorter will execute the callback and run without sorting then. This can be useful for example if you want to load a whole new table that is already sorted or contains new data.

## Help 

Feel free to contact as under `tablesorter@bitandblack.com`.